<?php namespace Decoupled\Core\Output;

use Decoupled\Core\Output\Output;
use Decoupled\Core\Action\ActionQueue;

class OutputHandler implements OutputHandlerInterface{

    public function getOutput( ActionQueue $actions )
    {
        $finalOutput = false;

        foreach( $actions->all() as $action )
        {
            $output = $action();

            if( $output instanceof Output )
            {
                $finalOutput = $output;
            }
        }

        return $finalOutput;
    }   
}