<?php namespace Decoupled\Core\Output;

use Decoupled\Core\Action\ActionQueue;

interface OutputHandlerInterface{

    public function getOutput( ActionQueue $actions );
}