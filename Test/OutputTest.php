<?php

require('../vendor/autoload.php');

use Decoupled\Core\Output\OutputHandler;
use phpunit\framework\TestCase;
use Decoupled\Core\Action\ActionFactory;
use Decoupled\Core\Action\ActionInvoker;
use Decoupled\Core\Action\ActionQueue;
use Decoupled\Core\Output\Output;

class OutputTest extends TestCase{

    public function testCanResolveOutput()
    {
        $queue = new ActionQueue();

        $actionFactory = new ActionFactory();

        $actionFactory->setInvoker( new ActionInvoker() );

        $out = new Output();

        $action = $actionFactory->make(function() use($out){

            $out->addParam( 'value', 1 );
        });

        $queue->add( $action );

        $action = $actionFactory->make(function() use($out){

            return $out('@app/template.php');
        });

        $queue->add( $action );

        $handler = new OutputHandler();

        $output = $handler->getOutput( $queue );

        $this->assertInstanceOf( Output::class, $output );

        $this->assertEquals( 1, $out->getParam( 'value' ) );

        $this->assertEquals( '@app/template.php', $output->getView() );
    }

}